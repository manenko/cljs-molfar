;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.game-state
  "Generic game state and routines.

  A game state is an immutable map. The entities can refer to each
other via IDs, where an ID is a vector of keys used to get access
to an entity in the game state map using `get-in`, `update-in`,
`assoc-in`, and friends.

All functions that alter the game state must be pure and return a
new game state that reflects the changes. These functions are used by
a top-level function that applies these changes to the game state
atom."
  (:require [clojure.spec.alpha :as spec]))

(spec/def ::id-key (comp not nil?))
(spec/def ::id     (spec/coll-of ::id-key :kind vector? :min-count 1))

(defn id
  "Returns an ID of the object of the game entity from the given
collection of keys.

If `key-or-id` is a vector, returns it with `ks` added. Otherwise,
returns a vector of `key-or-id` and `ks`.

```clojure
(id 1 2 3 4)
=> [1 2 3 4]

(id [1 2] 3 4)
=> [1 2 3 4]
```"
  [key-or-id & ks]
  (if (vector? key-or-id)
    (apply conj key-or-id ks)
    (into [] (conj ks key-or-id))))
