;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.rendering.tiled
  "Rendering support for tilemap-based games."
  (:require [manenko.molfar.core.rendering.renderer :as r]
            [manenko.molfar.core.shape              :as s]
            [manenko.molfar.core.tiled.tileset      :as ts]))

(defn render-tile!
  "Renders a tile with the given ID at the given coordinates using the given renderer."
  [game-state renderer tileset-id tile-id x y]
  (let [tileset   (get-in game-state tileset-id)
        image     (get-in game-state (:ts/image-id tileset))
        tile-rect (ts/tile-image-rect tileset tile-id)]
    (r/render-image! renderer
                     image
                     tile-rect
                     (s/make-rect x y (:width tile-rect) (:height tile-rect)))))
