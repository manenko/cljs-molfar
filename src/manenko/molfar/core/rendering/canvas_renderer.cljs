;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.rendering.canvas-renderer
  "Implementation of [[manenko.molfar.core.rendering.renderer/ARenderer]]
  protocol for `CanvasRenderingContext2D` Web API."
  (:require [goog.dom                               :as gdom]
            [manenko.molfar.core.rendering.renderer :as r]))

(defrecord CanvasRenderer [canvas context parent-renderer]
  r/ARenderer

  (r/output-size [this]
    (let [canvas (:canvas this)]
      [(.-width canvas)
       (.-height canvas)]))

  (r/scale-quality [this]
    ;; The `imageSmoothingQuality` property is experimental at the moment, and
    ;; we do not use it here.
    (if (.-imageSmoothingEnabled (:context this))
      :best
      :nearest))

  (r/set-scale-quality! [this scale-quality]
    ;; The `imageSmoothingQuality` property is experimental at the moment, and
    ;; we do not use it here.
    (letfn [(set-image-smoothing! [enable? success?]
              (set! (.-imageSmoothingEnabled (:context this)) enable?)
              success?)]
      (condp = scale-quality
        :nearest (set-image-smoothing! false true)
        :best (set-image-smoothing! true true)
        false)))

  (r/clear! [this]
    (let [[width height] (r/output-size this)]
      (.clearRect (:context this) 0 0 width height)))

  (r/render-image!
    [this image src-rect dst-rect]
    (r/render-image! this
                     image
                     (:left src-rect) (:top src-rect) (:width src-rect) (:height src-rect)
                     (:left dst-rect) (:top dst-rect) (:width dst-rect) (:height dst-rect)))
  (r/render-image!
    [this image src-x src-y src-width src-height dst-x dst-y dst-width dst-height]
    (.drawImage (:context this)
                image
                src-x src-y src-width src-height
                dst-x dst-y dst-width dst-height))

  (r/present! [this]
    (when-let [parent-renderer (:parent-renderer this)]
      (let [[dst-width dst-height] (r/output-size parent-renderer)
            [src-width src-height] (r/output-size this)]
        (r/clear! parent-renderer)
        (r/render-image! parent-renderer
                         (:canvas this)
                         0 0 src-width src-height
                         0 0 dst-width dst-height)))))

(defn make-renderer
  "Returns a [[manenko.molfar.core.rendering.renderer/ARenderer]] for the given
  Web canvas element.

  #### Arguments

  canvas
  : An HTML canvas element to render to.

  parent-renderer
  : An optional renderer which is used to render the `canvas` content when
  calling the [[manenko.molfar.core.rendering.renderer/present!]] method. The
  renderer must implement [[manenko.molfar.core.rendering.renderer/ARenderer]]
  protocol. This is useful for offscreen rendering."
  ([canvas]
   (make-renderer canvas nil))
  ([canvas parent-renderer]
   (->CanvasRenderer canvas (.getContext canvas "2d") parent-renderer)))

(defn make-offscreen-renderer
  "Returns an offscreen renderer of the given width and height. The renderer will
  draw its contents to the `parent-renderer` when calling
  the [[manenko.molfar.core.rendering.renderer/present!]] method."
  [width height parent-renderer]
  (let [offscreen-canvas (gdom/createElement "canvas")]
    (set! (.-width offscreen-canvas) width)
    (set! (.-height offscreen-canvas) height)
    (make-renderer offscreen-canvas parent-renderer)))
