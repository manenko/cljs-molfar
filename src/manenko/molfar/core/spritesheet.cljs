;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.spritesheet
  "Sprite sheets and texture atlases."
  (:require [manenko.molfar.core.rendering.renderer :as r]))

(defn- make-tile-properties
  [tile]
  (let [basic-properties  {:type (keyword (:type tile))
                           :id   (:id   tile)}
        custom-properties (reduce #(assoc %1 (keyword (:name %2)) (:value %2))
                                  {}
                                  (:properties tile))]
    (merge basic-properties custom-properties)))

(defn make-spritesheet
  "Creates a new spritesheet and stores it in the `game-state`."
  [game-state spritesheet-id image tileset]
  (assoc-in game-state
            spritesheet-id
            {:image        image
             :columns      (:columns tileset)
             :rows         (quot (:tilecount tileset)
                                 (:columns   tileset))
             :image-width  (:imagewidth  tileset)
             :image-height (:imageheight tileset)
             :tile-width   (:tilewidth   tileset)
             :tile-height  (:tileheight  tileset)
             :tiles        (reduce #(assoc %1 (:id %2) (make-tile-properties %2))
                                   {}
                                   (:tiles tileset))}))

(defn tile-column
  "Gets a tile column number in a spritesheet by its id."
  [ss tile-id]
  (mod tile-id (:columns ss)))

(defn tile-row
  "Gets a tile row number in a spritesheet by its id."
  [ss tile-id]
  (quot tile-id (:columns ss)))

(defn tile-x
  "Gets an X-coordinate of the top-left corner of the tile with the given id."
  [ss tile-id]
  (* (tile-column ss tile-id)
     (:tile-width ss)))

(defn tile-y
  "Gets a Y-coordinate of the top-left corner of the tile with the given id."
  [ss tile-id]
  (* (tile-row ss tile-id)
     (:tile-height ss)))

(defn tile-properties
  "Returns property map of the tile with the given id
  or nil if there are no properties associated with the tile."
  [ss tile-id]
  (get-in ss [:tiles tile-id]))

(defn tile-property
  "Returns a value of the given property or `not-found` (`nil` by default) if
  the property does not exist. "
  ([ss tile-id property-name]
   (tile-property ss tile-id property-name nil))
  ([ss tile-id property-name not-found]
   (get-in ss [:tiles tile-id property-name] not-found)))

(defn render-tile!
  "Renders a tile with the given id at the given coordinates using the given renderer."
  [renderer ss tile-id x y]
  (let [tx (tile-x ss tile-id)
        ty (tile-y ss tile-id)
        tw (:tile-width  ss)
        th (:tile-height ss)]
    (r/render-image! renderer
                     (:image ss)
                     tx ty tw th
                     x  y  tw th)))
