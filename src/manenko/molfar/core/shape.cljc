;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.shape
  "Geometric shapes and operations.")

(defrecord Point [x y])

(defn make-point
  "Returns a point from the given coordinates."
  [x y]
  (->Point x y))

(defrecord Rect [left top width height])

(defn make-rect
  "Returns a rectangle from the given coordinates
  of its top-left corner, width, and height."
  [left top width height]
  (->Rect left top width height))

(defn rect-right
  "Returns X-coordinate of the rectangle's right side."
  [rect]
  (+ (:left  rect)
     (:width rect)))

(defn rect-bottom
  "Returns Y-coordinate of the rectangle's bottom side."
  [rect]
  (+ (:top    rect)
     (:height rect)))

(defn rect-top-left
  "Returns coordinates of the top-left corner of the rectangle."
  [rect]
  (make-point (:left rect) (:top rect)))

(defn rect-top-right
  "Returns coordinates of the top-right corner of the rectangle."
  [rect]
  (make-point (rect-right rect) (:top rect)))

(defn rect-bottom-left
  "Returns coordinates of the bottom-left corner of the rectangle."
  [rect]
  (make-point (:left rect) (rect-bottom rect)))

(defn rect-bottom-right
  "Returns coordinates of the bottom-right corner of the rectangle."
  [rect]
  (make-point (rect-right rect) (rect-bottom rect)))

(defrecord Ellipse [x y width height])

(defn make-ellipse
  "Returns a new ellipse from the given coordinates of its center, width, and height."
  [x y width height]
  (->Ellipse x y width height))
