;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.creature
  "Game NPCs and player-controlled characters."
  (:require [manenko.molfar.core.rendering.renderer :as r]
            [manenko.molfar.core.spritesheet        :as ss]
            [manenko.molfar.core.tilemap            :as tm]))

(defn make-creature
  "Creates a creature and adds it to the game state under the given `creature-id`.

  **Arguments**

  game-state
  : The global game state.

  creature-id
  : The vector of keys that refers to a place in the `game-state` to store the creature.

  spritesheet-id
  : The vector of keys that refers to the creature's spritesheet in the `game-state`.

  speed
  : The movement speed of the creature in pixels per second.

  animations
  : The map of the creature animations.

  **Creature Entity Structure**

  | Key               | Description                                                   |
  |-------------------|---------------------------------------------------------------|
  | `:x`              | X-coordinate of the top left corner of the creature tile.     |
  | `:y`              | Y-coordinate of the top left corner of the creature tile.     |
  | `:screen-x`       | Same as `:x` but in screen coordinates (camera's viewport).   |
  | `:screen-y`       | Same as `:y` but in screen coordinates (camera's viewport).   |
  | `:width`          | The width of the creature tile.                               |
  | `:height`         | The height of the creature tile.                              |
  | `:speed`          | The movement speed of the creature in pixels per second.      |
  | `:tilemap-id`     | The id of the tilemap to spawn the creature into.             |
  | `:spritesheet-id` | The spritesheet that contains the creature tiles.             |
  | `:direction`      | The movement direction: `:north`, `:south`, `:west`, `:east`. |
  | `:status`         | `:moving`, `:standing`.                                       |
  | `:animations`     | The map that describes the creature animations.               |"
  [game-state creature-id spritesheet-id speed animations]
  (let [spritesheet (get-in game-state spritesheet-id)
        width       (:tile-width  spritesheet)
        height      (:tile-height spritesheet)]
    (assoc-in game-state
              creature-id
              {:x              0
               :y              0
               :screen-x       0   ;; Screen coordinates are updated
               :screen-y       0   ;; by the current camera
               :width          width
               :height         height
               :tilemap-id     nil ;; It will be set once we spawn the creature in a tilemap
               :spritesheet-id spritesheet-id
               :direction      :south
               :status         :standing
               :animations     animations
               :speed          speed})))

(defn spawn-creature
  "Spawns the given creature in the given game world at the given coordinates.

  **Arguments**

  game-state
  : The global game state.

  creature-id
  : The vector of keys that refers to the creature entity in the `game-state`.

  tilemap-id
  : The vector of keys that refers to the tilemap entity in the `game-state`.

  x
  : The X-coordinate of the top left corner of the creature sprite.

  y
  : The Y-coordinate of the top left corner of the creature sprite."
  [game-state creature-id tilemap-id x y]
  (-> game-state
      (assoc-in (conj creature-id :tilemap-id) tilemap-id)
      (assoc-in (conj creature-id :x)          x)
      (assoc-in (conj creature-id :y)          y)))

(defn- animation-key
  "Returns animation key for the given character movement direction and status."
  [direction status]
  (keyword (str (if (= status :standing) "looking" "moving") "-" (name direction))))

(defn- animation-frame [creature]
  (let [akey (animation-key (:direction creature) (:status creature))]
    (get-in creature [:animations akey :frames 0])))

(defn- direction [dir-x dir-y current-direction]
  (cond
    ;; TODO: Handle north-west, north-east, etc. Requires additional art.
    (= -1 dir-x) :west
    (=  1 dir-x) :east
    (= -1 dir-y) :north
    (=  1 dir-y) :south
    :else        current-direction))

(defn- status [dir-x dir-y]
  (if (= 0 dir-x dir-y) :standing :moving))

(defn render-creature!
  "Renders the creature at its current position."
  [game-state renderer creature-id]
  (let [creature    (get-in game-state creature-id)
        spritesheet (get-in game-state (:spritesheet-id creature))]
    (ss/render-tile! renderer
                     spritesheet
                     (animation-frame creature)
                     (- (:screen-x creature) (/ (:width  creature) 2))
                     (- (:screen-y creature) (/ (:height creature) 2)))))

(defn collide-creature
  "Performs collision simulation for a creature in the game world and updates the
  creature's coordinates to prevent the creature passing through the obstacles.

  **Arguments**

  game-state
  : The global game state.

  creature-id
  : The vector of keys used to access the creature in the `game-state` to perform
  collision simulation on.

  previous-x
  : The X-coordinate of the previous creature position before it moved.

  previous-y
  : The Y-coordinate of the previous creature position before it moved.

  **Algorithm**

  We have a creature in the world:

  ```
  ┌───┐
  │ ▜ ◀───Creature
  └───┘
  ```

  We want to check if the creature can move to any of the surrounding tiles. If
  a tile is an obstacle, the creature cannot move in the direction of the tile.

  For example, on the following diagram the creature cannot move in neither of
  west or south directions because of the blocking tiles:

  ```
     No obstacles
        │   │
        │   │
      ┌─▼─┐ │
      │   │ │
  ┌───┼───┼─▼─┐
  │███│ ▜ │   │
  └─▲─┼───┼───┘
    │ │███│
    │ └─▲─┘
    │   │
    │   │
  Obstacles
  ```

  If the creature's tile rect intersects with any blocking tile, the creature
  cannot move in the respective direction(-s) and its coordinates have to be
  adjusted. So the typical approach would be checking if tile rects intersect
  using the [[manenko.molfar.collision/rects-intersect?]] function.

  However, in this particular case we can use a simpler approach:

  1. Compute coordinates of each corner for the creature tile rectangle.
  2. Get tile at each corner.
  3. Check if the tile is an obstacle.
  4. Ajdust creature position by resetting it to the previous position.

  The steps 1-3 are collision *detection* and step 4 is a collision *response*.

  The collision response described in the step 4 is naive. It has problems when
  the creature is moving in both X and Y coordinates: instead of halting in the
  collision direction, and continuing to slide along the obstacle in the other
  direction, all movement stops.

  To solve this problem we have to do separate collision detection and response
  for X and Y movement. That is if there is an obstacle along the X axis we revert
  X-coordinate and then check Y, if there is no obstacle there, we allow movement
  in Y axis.

  **Known Issues**

  A creature is blocked from passing through one-tile-width corridor at the
  moment:

  ```
         ┌───┬───┬───┐
         │   │ ▜ │   │
         ├───┼─┬─┼───┤
         │███│ │ │███│
         ├───┼─┼─┼───┤
         │███│ x │███│
         └───┴─┼─┴───┘
               │
               ▼
  Cannot move through the corridor
  ```

  I have to investigate why this is happening."
  [game-state creature-id previous-x previous-y]
  (let [creature    (get-in game-state creature-id)]
    (if (= :standing (:status creature))
      ;; If there is no movement, return the original state
      game-state
      ;; There is a movement, perform collision detection and reaction
      (let [tilemap     (get-in game-state (:tilemap-id     creature))
            spritesheet (get-in game-state (:spritesheet-id tilemap))

            ;; Cache current properties of the creature
            creature-x  (:x         creature)
            creature-y  (:y         creature)
            width       (:width     creature)
            height      (:height    creature)
            direction   (:direction creature)

            half-width  (/ width  2)
            half-height (/ height 2)

            ;; Compute coordinates of the creature tile corners
            left        (- creature-x half-width)
            top         (- creature-y half-height)
            right       (+ creature-x half-width)
            bottom      (+ creature-y half-height)

            collision?  (or (tm/obstacle-at-xy? tilemap spritesheet left  top)
                            (tm/obstacle-at-xy? tilemap spritesheet right top)
                            (tm/obstacle-at-xy? tilemap spritesheet left  bottom)
                            (tm/obstacle-at-xy? tilemap spritesheet right bottom))]
        (if collision?
          (-> game-state
              (assoc-in (conj creature-id :x) previous-x)
              (assoc-in (conj creature-id :y) previous-y))
          ;; There is no collision, return the state unchanged
          game-state)))))

(defn move-creature
  "Moves the creature in the given direction(-s).

  **Arguments**

  game-state
  : The global game state.

  creature-id
  : The vector of keys used to extract the creature entity from the `game-state`.

  dir-x
  : Whether the creature should move to the west, east, or none. Refer to the
  **Remarks** section for more information.

  dir-y
  : Whether the creature should move to the north, south, or none. Refer to the
  **Remarks** section for more information.

  dt
  : Delta time since the last update.

  **Remarks**

  The function checks for collisions and does not move the creature in case of
  colliding with an obstacle.

  The distance the creature moves depends on the time passed since the last game
  loop iteration - `dt`.

  The moving direction depends on the `dir-x` and `dir-y` as described in the
  following tables.

  | `dir-x` | Action                                    |
  |---------|-------------------------------------------|
  | -1      | Move the creature in the west direction.  |
  |  0      | Do not move the creature horizontally.    |
  | +1      | Move the creature in the east direction.  |

  | `dir-y` | Action                                    |
  |---------|-------------------------------------------|
  | -1      | Move the creature in the north direction. |
  |  0      | Do not move the creature vertically.      |
  | +1      | Move the creature in the south direction. |"
  [game-state creature-id dir-x dir-y dt]
  (let [creature   (get-in game-state creature-id)
        tilemap    (get-in game-state (:tilemap-id creature))

        ;; The creature cannot move outside of the map.
        max-x      (tm/tilemap-width  tilemap)
        max-y      (tm/tilemap-height tilemap)

        prev-x     (:x creature)
        prev-y     (:y creature)

        ;; Compute the new position from  the speed, direction, and time passsed
        ;; since the last update.
        x          (+ (:x creature) (* dir-x (:speed creature) dt))
        y          (+ (:y creature) (* dir-y (:speed creature) dt))

        ;; Clamp the new position, making sure the creature
        ;; has not moved outside of the game world.
        x          (max 0 (min x max-x))
        y          (max 0 (min y max-y))

        ;; Compute the character's  movement direction and status.  We will need
        ;; them for animations during the rendering phase.
        st         (status    dir-x dir-y)
        dir        (direction dir-x dir-y (:direction creature))

        game-state (-> game-state
                       (assoc-in (conj creature-id :status) st)
                       (assoc-in (conj creature-id :direction) dir)
                       ;; Update X-position.
                       (assoc-in (conj creature-id :x) x)
                       ;; Revert the creature's position to (`prev-x`, `prev-y`)
                       ;; if there is a collision with an obstacle.
                       (collide-creature creature-id prev-x prev-y)
                       ;; Update Y position.
                       (assoc-in (conj creature-id :y) y))]
    ;; Revert the creature's position to (`current-x`, `prev-y`)
    ;; if there is a collision with an obstacle. The `current-x`
    ;; is either `prev-x` there was a collision when updating X
    ;; coordinate, or `x` if there was no collision.
    (collide-creature game-state
                      creature-id
                      (get-in game-state (conj creature-id :x)) prev-y)))
