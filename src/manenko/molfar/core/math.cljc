;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.math
  "Mathematical functions."
  #?(:clj (:require [clojure.math :as cm])))

(defn floor
  "Returns the largest double less than or equal to `d`, and equal to a
   mathematical integer."
  ^double [^double d]
  #?(:clj  (cm/floor d)
     :cljs (.floor js/Math d)))

(defn ceil
  "Returns the smallest double greater than or equal to `d`, and equal to
  a mathematical integer."
  ^double [^double d]
  #?(:clj  (cm/ceil d)
     :cljs (.ceil js/Math d)))

(defn round
  "Returns the closest long to `d`."
  ^long [^double d]
  #?(:clj  (cm/round d)
     :cljs (.round js/Math d)))
