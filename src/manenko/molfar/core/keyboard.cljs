;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.keyboard
  "Keyboard input handlers.")

(def +keycode-left+  "Keyboard left arrow."   37)
(def +keycode-up+    "Keyboard up arrow."     38)
(def +keycode-right+ "Keyboard right arrow."  39)
(def +keycode-down+  "Keyboard down arrow."   40)

(def ^:private +known-keycodes+
  "Keycodes known to the game will be processed by this module, everything else
  will be ignored."
  #{+keycode-left+
    +keycode-up+
    +keycode-right+
    +keycode-down+})

(defn- known-keycode? [keycode]
  (+known-keycodes+ keycode))

(def ^:private keyboard-state  "A set of keys pressed."  (atom #{}))

(defn- on-key-pressed [event]
  (let [keycode (.-keyCode event)]
    (when (known-keycode? keycode)
      (.preventDefault event)
      (swap! keyboard-state conj keycode))))

(defn- on-key-released [event]
  (let [keycode (.-keyCode event)]
    (when (known-keycode? keycode)
      (.preventDefault event)
      (swap! keyboard-state disj keycode))))

(defn subscribe-to-keyboard-events!
  "Starts listening to the keyboard events of the JS window."
  []
  (.addEventListener js/window "keydown" on-key-pressed)
  (.addEventListener js/window "keyup"   on-key-released))

(defn key-pressed?
  "Checks if the key with the given keycode is currently being pressed."
  [keycode]
  (when-not (known-keycode? keycode)
    (.warn js/console (str "Keycode " keycode " is not being listened to.")))
  (@keyboard-state keycode))

(defn key-released?
  "Checks if the key with the given keycode is currently being released."
  [keycode]
  (not (key-pressed? keycode)))
