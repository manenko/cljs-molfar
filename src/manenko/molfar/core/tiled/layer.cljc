;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.tiled.layer
  "Layers organize/group tilemaps' content. The order of the layers
  determines the rendering order of the content.

  ## Layer Types

  There are three layer types supported by the engine:

  - tile layers
  - object layers
  - group layers

  ### Tile Layers

  Tile layers provide an efficient way of storing large area filled
  with tiles. The tiles are stored as a vector of global tile IDs and a
  few flags.

  The tilemap stores the order in which layers should be rendered. A
  layer could be made invisible in which case it will not be considered
  for rendering.

  Most of the time, tile layers are used to represent graphical
  information which should be rendered at some point. However, they
  could represent non-graphical information too. For example, collision
  information could be stored in the map in a separate tile layer using
  a special tileset. Then the game will check the layer when performing
  a collision detection.

  ### Object Layers

  Object layers store various kind of information in a form of
  objects. There are a few types of objects:

  `Rectangle`
  : For marking rectangular areas.

  `Ellipse`
  : For marking ellipse or circular area.

  `Point`
  : For marking exact locations.

  `Polygon`
  : When a rectangle or ellipse is not enough.

  `Polyline`
  : For marking waypoints, walls, etc.

  All objects can have a name and custom properties. Their width,
  height, and position are measured in pixels.

  ### Group Layers

  Group layers are folders used to organize layers into a hierarchy.
  The visibility of a group layer affects visibility of all its child
  layers.

  ## Anatomy of a Global Tile ID

  A global tile ID is an integer number. Negative values mean 'no tile'.

  An ID could have a few flags, that allow tile images to be flipped
  vertically, horizontally or anti-diagonally. The flags are applied
  to the number via bitwise `or` operation."
  (:require [manenko.molfar.core.math           :as math]
            [manenko.molfar.core.shape          :as s]
            [manenko.molfar.core.tiled.property :as tp]))

(defmulti make-object
  "Returns an object layer object created from the Tiled editor's
   `object`.

   ##### Object Keys

   The following are the keys applicable to any object in an object
   layer.

   `::id`
   : An object ID unique across all objects in a tilemap.

   `::name`
   : The name of the object.

   `::type`
   : An optional custom object type. It is assigned in the editor.

   `::rotation`
   : Rotation angle in degrees.

   `::properties`
   : An optional vector of the object properties. Refer to the
   [[manenko.molfar.core.tiled.property/make-property]] function
   documentation for the property map structure.

   `::shape`
   : The shape of the object. Could be one of `::ellipse`, `::point`,
   `::polygon`, `::polyline`, `::rectangle`, `::tile`.

   `::value`
   : The data specific for this object `::shape`:
     - `::ellipse`: the ellipse entity. Refer to the
       [[manenko.molfar.shape/make-ellipse]] function for more details.
     - `::point`: the point entity. Refer to the
       [[manenko.molfar.shape/make-point]] function for more details.
     - `::polygon`: the vector of point entities. Refer to the
       [[manenko.molfar.shape/make-point]] function for the point entity
       structure.
     - `::polyline`: the vector of point entities. Refer to the
       [[manenko.molfar.shape/make-point]] function for the point entity
       structure.
     - `::rectangle`: the rectangle entity. Refer to the
       [[manenko.molfar.shape/make-rect]] function for more details.
     - `::tile`: the tile 'shape'. The tile shape is a map of the tile
       global ID – `::id` and coordinates of its top-left corner –
       `::x` and `::y`."
  (fn [object]
    (cond
      (:ellipse  object) :ellipse
      (:point    object) :point
      (:polygon  object) :polygon
      (:polyline object) :polyline
      (:text     object) :text
      (:gid      object) :tile
      :else              :rectangle)))

(defn- premake-object
  "Returns an object layer object filled with common properties from
   the given Tiled editor's `object`."
  [object]
  {::id         (:id       object)
   ::type       (:type     object)
   ::name       (:name     object)
   ::rotation   (:rotation object)
   ::properties (mapv tp/make-property (:properties object))})

(defn- make-object'
  "Returns an object layer object created from the given Tiled editor's
   `object`.

  This is a helper function and should not be used outside of the
  namespace."
  [object shape value]
  (-> object
      premake-object
      (assoc ::shape shape
             ::value value)))

(defmethod make-object :ellipse
  [object]
  (make-object' object
                ::ellipse
                (s/make-ellipse (:x      object)
                                (:y      object)
                                (:width  object)
                                (:height object))))

(defmethod make-object :point
  [object]
  (make-object' object
                ::point
                (s/make-point (:x object) (:y object))))

(defmethod make-object :polygon
  [object]
  (make-object' object
                ::polygon
                (mapv #(s/make-point (:x %) (:y %))
                      (:polygon object))))

(defmethod make-object :polyline
  [object]
  (make-object' object
                ::polyline
                (mapv #(s/make-point (:x %) (:y %)) (:polyline object))))

(defmethod make-object :tile
  [object]
  (make-object' object
                ::tile
                {::id (:gid object)
                 ::x  (:x object)
                 ::y  (:y object)}))

(defmethod make-object :rectangle
  [object]
  (make-object' object
                ::rectangle
                (s/make-rect (:x      object)
                             (:y      object)
                             (:width  object)
                             (:height object))))

(defmethod make-object :default
  [_])

(defmulti ^:private make-layer'
  "Returns a tilemap layer created from the Tiled editor's `layer`.

  The engine supports the following layer types:

  - `group`
  - `objectgroup`
  - `tilelayer`"
  (fn [layer] (:type layer)))

(defn- premake-layer
  "Returns a layer filled with common properties from the given Tiled
   editor's `layer`."
  [layer]
  {::id         (:id      layer)
   ::name       (:name    layer)
   ::visible?   (:visible layer)
   ::properties (mapv tp/make-property (:properties layer))})

(defmethod make-layer' "objectgroup"
  [layer]
  (-> layer
      premake-layer
      (assoc ::type    ::object-layer
             ::objects (->> (:objects layer)
                            (map    make-object)
                            (filter identity)
                            (into   [])))))

(def ^{:private true, :const true} +flipped-horizontally+  0x80000000)
(def ^{:private true, :const true} +flipped-vertically+    0x40000000)
(def ^{:private true, :const true} +flipped-diagonally+    0x20000000)
(def ^{:private true, :const true} +rotated-hexagonal-120+ 0x10000000)
(def ^{:private true, :const true} +all-flags+             (bit-or
                                                            +flipped-horizontally+
                                                            +flipped-vertically+
                                                            +flipped-diagonally+
                                                            +rotated-hexagonal-120+))
(def ^{:private true, :const true} +all-flags-clear-mask+  (bit-not +all-flags+))

(defn clear-gid-flags
  "Returns the global tile ID with the flip flags cleared.

  The highest four bits of the 32-bit global tile ID are flip flags and
  has to be cleared to get the global tile ID itself.

  Currently, the engine has no support of flipped tiles."
  [gid]
  (bit-and gid +all-flags-clear-mask+))

(defmethod make-layer' "tilelayer"
  [layer]
  (-> layer
      premake-layer
      (assoc ::type    ::tile-layer
             ::tiles   (mapv clear-gid-flags (:data layer))
             ::columns (:width  layer)
             ::rows    (:height layer))))

(defmethod make-layer' "group"
  [layer]
  (-> layer
      premake-layer
      (assoc ::type    ::group
             ::layers  (mapv make-layer' (:layers layer)))))

(defmethod make-layer' :default
  [_])

(defn make-layer
  "The single-arity version creates a new layer from the given Tiled
  Editor layer data and returns it.

  The three-arity function creates a new layer from the given Tiled
  Editor layer data and returns a new game state with the layer added.

  ##### Arguments

  `game-state`
  : The current game state. Must be a map.

  `layer-id`
  : The vector of keys used to store the layer in the game state map.

  `tsj-layer`
  : The layer data parsed from the Tiled JSON tileset format. The keys in
  the data must be converted to keywords.

  ##### Common Layer Keys

  The following are the keys that applicable to any type of layer.

  `::id`
  : The layer ID. It is unique across all layers in the tilemap.

  `::name`
  : The name of the layer.

  `::type`
  : The type of the layer. Could be either of `::tile-layer`,
  `::object-layer`, or `::group`.

  `::visible?`
  : Controls layers visibility. Invisible tile layers are not rendered.
  Invisible group layers make all child layers invisible, even if child
  layers have this property set to `true`. This property is ignored by the
  engine for object layers but a game may use it.

  `::properties`
  : An optional vector of the layer properties. Refer to the
  [[manenko.molfar.core.tiled.property/make-property]] function
  documentation for the property map structure.

  ##### Tile Layer Keys

  `::tiles`
  : A vector of global tile IDs.

  `::columns`
  : The number of columns in the layer.

  `::rows`
  : The number of rows in the layer.

  ##### Object Layer Keys

  `::objects`
  : A vector of objects in the layer. Refer to
  [[manenko.molfar.core.tiled.layer/make-object]] function documentation
  for the object map structure.

  ##### Group Layer Keys

  `::layers`
  : A vector of child layers."
  ([tsj-layer]
   (make-layer' tsj-layer))
  ([game-state layer-id tsj-layer]
   (assoc-in game-state layer-id (make-layer tsj-layer))))

(defn empty-tile?
  "Checks whether the given tile is empty and should not be rendered."
  [gid]
  (<= gid 0))

(defn tile-gid
  "Returns a tile global ID at the given column and row for the given
   tile layer. Returns zero if there is no tile found."
  [layer column row]
  (let [columns (::columns layer)
        index   (+ column (* row columns))]
    (nth (::tiles layer) index 0)))

(defn tile-gid-at-point
  "Returns a tile global ID at the given coordinates in pixels. Returns
   zero if there is no tile found."
  ([layer tile-width tile-height point]
   (tile-gid-at-point layer tile-width tile-height (:x point) (:y point)))
  ([layer tile-width tile-height x y]
   (tile-gid layer
             (math/floor (/ x tile-width))
             (math/floor (/ y tile-height)))))
