;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.tiled.property
  "Custom properties for tiles, tilemaps, tilesets, etc."
  (:require [clojure.spec.alpha :as spec]))

(spec/def ::name keyword?)
(spec/def ::type #{:string
                   :int
                   :float
                   :bool
                   :color
                   :file
                   :object
                   :class})

(defn make-property
  "Returns a custom property map.

  The single-arity version creates a property from a map parsed from a Tiled JSON
  property.

  The three-arity version creates a property from the given property name, type,
  and value."
  ([tjs-property]
   (make-property (:name  tjs-property)
                  (:type  tjs-property)
                  (:value tjs-property)))
  ([name type value]
   {::name  (keyword name)
    ::type  (keyword type)
    ::value value}))
