;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.tiled.tileset
  "Tilesets are a type of spritesheet, and the sprites (tiles) within them are
  expected to be put together side by side into some larger shape rather than
  used in isolation or as part of a sequence of images (i.e. an animation;
  although tiles can also be animated)."
  (:require [clojure.spec.alpha                 :as spec]
            [manenko.molfar.core.tiled.property :as tp]
            [manenko.molfar.core.shape          :as s]))

(spec/def ::tile-id   int?)
(spec/def ::tile-type (spec/nilable string?))
(spec/def ::duration  int?)

(defn make-tile-metadata
  "Returns a tile's metadata map.

  The single-arity function reads metadata from the given map of Tiled JSON tile data.

  The four-arity function creates the metadata map from the given tile local ID,
  type, vector of properties, and vector of animation frames.

  ##### Arguments

  `tsj-tile-data`
  : The map of a tile object parsed from the Tiled JSON tileset. The keys must
  be keywords.

  `id`
  : The tile's local ID.

  `type`
  : An optional tile type.

  `properties`
  : An optional vector of tile
  properties (see [[manenko.molfar.core.tiled.property/make-property]] for the
  property map structure).

  `animation`
  : An optional vector of tile animation frames. An animation frame is a map of
  two keys: `::tile-id` and `::duration`. The former is the
  local tile ID and the latter is animation frame duration in milliseconds."
  ([tsj-tile-data]
   (make-tile-metadata (:id   tsj-tile-data)
                       (:type tsj-tile-data)
                       (mapv tp/make-property
                             (:properties tsj-tile-data))
                       (mapv #({::tile-id  (:tileid   %)
                                ::duration (:duration %)})
                             (:animation tsj-tile-data))))
  ([id type properties animation]
   {::tile-id    id
    ::tile-type  type
    ::properties properties
    ::animation  animation}))

(defn make-tileset
  "The single-arity version creates a new tileset from the given Tiled Editor
  tileset data and returns it.

  The three-arity function creates a new tileset from the given Tiled Editor
  tileset data and returns a new game state with the tileset added.

  ##### Arguments

  `game-state`
  : The current game state. Must be a map.

  `tileset-id`
  : The vector of keys used to store the tileset in the game state map.

  `tsj-data`
  : The tileset data parsed from the Tiled JSON tileset format. The keys in the
  data must be converted to keywords.

  ##### Tileset Keys

  `::image-id`
  : The unique identifier of the tileset image in the global game state. This is
  a vector of keys used to access/update the image via `get-in`, `assoc-in` and
  friends. It is absent from the map returned by this function and has to be
  added externally once the image resource is ready and stored in the game
  state.

  `::image-location`
  : The location of the image resource. This should be used to read the image
  data by some external code and store it in the global game state.

  `::image-width`
  : The width of the tileset image in pixels.

  `::image-height`
  : The height of the tileset image in pixels.

  `::columns`
  : The number of tile columns in the tileset.

  `::rows`
  : The number of tile columns in the tileset.

  `::tile-width`
  : The width of a tile in the tileset in pixels. All tiles have the same width.

  `::tile-height`
  : The height of a tile in the tileset in pixels. All tiles have the same
  height.

  `::tile-offset-x`
  : The horizontal offset in pixels to be applied when drawing a tile from the
  tileset.

  `::tile-offset-y`
  : The vertical offset in pixels to be applied when drawing a tile from the
  tileset.

  `::spacing`
  : The spacing between adjacent tiles in the tileset image in pixels.

  `::first-gid`
  : The global identifier (GID) corresponding to the first tile in the tileset.
  This value is stored in a tilemap file and should be read from there.

  `::tiles`
  : The map of tile metadata. Some of tileset properties may have additional
  data and custom properties associated with them. This map associates local
  tile IDs to such data.

  ##### Local Tile Identifier vs Global Tile Identifier

  Each tileset assigns a unique number to each tile. These numbers are unique
  inside the tileset but a tilemap could use multiple tilesets and should have a
  way to distinguish between local tile identifiers of different tilesets.

  Tilemaps use global tile identifiers. Each tileset gets a unique *first global
  identifier* number or `first-gid`. The tilemap adds this number to a tile's
  local identifier to get a global tile identifier, i.e.:

  ```
  global-tile-id = first-gid + local-tile-id
  ```

  Tileset files have no `first-gid`s defined because they are specific to
  tilemaps which generate and assign them to tilesets they use. This is why
  `first-gid` is absent from the map returned by this function.

  ##### External vs Embedded Tilesets

  A tilemap can embed tilesets, storing all its data (except the image).
  Oftentimes tilesets are stored in a separate file/resource so that
  multiple tilemaps can share them. The engine supports external tilesets
  only."
  ([tsj-data]
   {::image-location (:image        tsj-data)
    ::image-width    (:imagewidth   tsj-data)
    ::image-height   (:imageheight  tsj-data)
    ::columns        (:columns      tsj-data)
    ::tile-width     (:tilewidth    tsj-data)
    ::tile-height    (:tileheight   tsj-data)
    ::spacing        (:spacing      tsj-data)
    ::tile-offset-x  (get-in tsj-data [:tileoffset :x])
    ::tile-offset-y  (get-in tsj-data [:tileoffset :y])
    ::rows           (/ (:tilecount tsj-data)
                        (:columns   tsj-data))
    ::tiles          (into {} (map #(fn [tile] {(:id tile) (make-tile-metadata tile)})
                                   (:tiles tsj-data)))})
  ([game-state tileset-id tsj-data]
   (assoc-in game-state tileset-id (make-tileset tsj-data))))

(defn tile-column
  "Returns a tile column number in the `tileset` by its ID."
  [tileset tile-id]
  (mod tile-id (::columns tileset)))

(defn tile-row
  "Returns a tile row number in the `tileset` by its ID."
  [tileset tile-id]
  (quot tile-id (::columns tileset)))

(defn tile-x
  "Returns an X-coordinate of the top-left corner of the tile with the given ID."
  [tileset tile-id]
  (* (tile-column tileset tile-id)
     (::tile-width tileset)))

(defn tile-y
  "Returns a Y-coordinate of the top-left corner of the tile with the given ID."
  [tileset tile-id]
  (* (tile-row tileset tile-id)
     (::tile-height tileset)))

(defn tile-image-rect
  "Returns an image rectangle for the given tile."
  [tileset tile-id]
  (s/make-rect (tile-x tileset tile-id)
               (tile-y tileset tile-id)
               (::tile-width  tileset)
               (::tile-height tileset)))

(defn tile-property
  "Returns the value of the property or `not-found` (`nil` by default) if
  the property does not exist."
  ([tileset tile-id property-name]
   (tile-property tileset tile-id property-name nil))
  ([tileset tile-id property-name not-found]
   (get-in tileset
           [::tiles tile-id ::properties property-name ::value]
           not-found)))
