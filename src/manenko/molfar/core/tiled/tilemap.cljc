;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.tiled.tilemap
  "Tilemaps are arrangements of tiles forming meaningful shapes and patterns, such
  as environments or puzzle layouts.

  A tilemap has the following (high-level) structure:

```clojure
   ╔═══════╗
   ║Tilemap║
   ╚═══════╝
       │
       │
       │
       │                Vector of custom properties. They are created by
       │                            an artist in the editor.
       │               ┌─────────────────────────────────────────────────┐
       │               │┌────────────────────┐     ┌────────────────────┐│
       │               ││         0          │     │         N          ││
       │               │├───────┬────────────┤     ├───────┬────────────┤│
       │               ││ ::name│:bonus-map? │     │ ::name│:max-gold   ││
       │             ┌▶│├───────┼────────────┤ ... ├───────┼────────────┤│
       │             │ ││ ::type│::bool      │     │ ::type│::int       ││
       │             │ │├───────┼────────────┤     ├───────┼────────────┤│
       │             │ ││::value│true        │     │::value│3000        ││
       │             │ │└───────┴────────────┘     └───────┴────────────┘│
       │             │ └─────────────────────────────────────────────────┘
       │             │
       │             │
       │             │              Vector of tilesets used in the tilemap layers.
       │             │     ┌───────────────────────────────────────────────────────────────┐
       │             │     │┌────────────────────────────┐   ┌────────────────────────────┐│
       │             │     ││             0              │   │             N              ││
       │             │     │├────────────────┬───────────┤   ├────────────────┬───────────┤│
       ▼             │     ││::image-location│/img/st.png│   │::image-location│/img/tg.png││
┌─────────────┬────┐ │     │├────────────────┼───────────┤   ├────────────────┼───────────┤│
│    ::columns│ 32 │ │     ││   ::image-width│512        │   │   ::image-width│128        ││
├─────────────┼────┤ │     │├────────────────┼───────────┤   ├────────────────┼───────────┤│
│       ::rows│ 16 │ │     ││  ::image-height│512        │   │  ::image-height│128        ││
├─────────────┼────┤ │     │├────────────────┼───────────┤   ├────────────────┼───────────┤│
│ ::tile-width│ 64 │ │ ┌──▶││    ::tile-width│64         │...│    ::tile-width│64         ││
├─────────────┼────┤ │ │   │├────────────────┼───────────┤   ├────────────────┼───────────┤│
│::tile-height│ 64 │ │ │   ││   ::tile-height│64         │   │   ::tile-height│64         ││
├─────────────┼────┤ │ │   │├────────────────┼───────────┤   ├────────────────┼───────────┤│
│ ::properties│ [] │─┘ │   ││       ::columns│8          │   │       ::columns│2          ││
├─────────────┼────┤   │   │├────────────────┼───────────┤   ├────────────────┼───────────┤│
│   ::tilesets│ [] │───┘   ││          ::rows│8          │   │          ::rows│2          ││
├─────────────┼────┤       │├────────────────┼───────────┤   ├────────────────┼───────────┤│
│     ::layers│ [] │       ││         ::tiles│{}         │──┐│         ::tiles│{}         ││
└─────────────┴────┘       │└────────────────┴───────────┘  │└────────────────┴───────────┘│
                           └────────────────────────────────┼──────────────────────────────┘
                           ┌────────────────────────────────┘
                           │                 ┌─────────────────────────────────────────────┐
                           │  Map of local   │┌──────────────────┐     ┌──────────────────┐│
                           │  tile IDs to    ││        0         │     │        N         ││
                           │their properties │├───────┬──────────┤     ├───────┬──────────┤│
                           │  ┌───────────┐  ││ ::name│:obstacle?│     │ ::name│:pickable?││
                           │  │┌────┬────┐│  │├───────┼──────────┤ ... ├───────┼──────────┤│
                           │  ││ 3  │ [] │├─▶││ ::type│::bool    │     │ ::type│::bool    ││
                           └─▶│├────┼────┤│  │├───────┼──────────┤     ├───────┼──────────┤│
                              ││ 7  │ [] ││  ││::value│true      │     │::value│false     ││
                              │└────┴────┘│  │└───────┴──────────┘     └───────┴──────────┘│
                              └───────────┘  └─────────────────────────────────────────────┘
```"
  (:require [manenko.molfar.core.math               :as math]
            [manenko.molfar.core.tiled.property     :as tp]
            [manenko.molfar.core.tiled.layer        :as tl]
            [manenko.molfar.core.tiled.tileset      :as ts]
            [manenko.molfar.core.rendering.renderer :as r]))

(defn make-tilemap
  "The two-arity version creates a new tilemap from the given Tiled Editor
  tilemap data and returns it.

  The four-arity function creates a new tilemap from the given Tiled Editor
  tilemap data and returns a new game state with the tilemap added.

  ```clojure
  (-> game-state
      (tiled/make-tilemap [::tilemaps \"dungeon\"]
                          (partial conj [::tilesets])
                          tsj-tilemap)
      (my-code/load-tilesets))
  ```

  ##### Arguments

  `game-state`
  : The current game state. Must be a map.

  `tilemap-id`
  : The vector of keys used to store the tileset in the game state map.

  `make-tileset-id-fn`
  : A function of one argument that accepts a location of an external
  tileset and returns a vector of keys that points to the tileset in the
  `game-state`. It is not required for the tileset to exist in the game
  state at this point. The `make-tilemap` function does not load the map's
  tilesets, it exposes their locations so that external code can load them
  and store in the game state under the keys returned by
  `make-tileset-id-fn`.

  `tsj-tilemap`
  : The tilemap data parsed from the Tiled JSON tileset format. The keys in the
  data must be converted to keywords."
  ([make-tileset-id-fn tsj-tilemap]
   {:pre [(= "orthogonal" (:orientation tsj-tilemap))
          (not (:infinite tsj-tilemap))]}
   {::columns     (:width      tsj-tilemap)
    ::rows        (:height     tsj-tilemap)
    ::tile-width  (:tilewidth  tsj-tilemap)
    ::tile-height (:tileheight tsj-tilemap)
    ::properties  (mapv tp/make-property (:properties tsj-tilemap))
    ::layers      (mapv tl/make-layer    (:layers     tsj-tilemap))
    ::tilesets    (-> (:tilesets tsj-tilemap)
                      (sort-by :firstgid)
                      (mapv (fn [t] {::first-gid  (:firstgid t)
                                     ::tileset-id (make-tileset-id-fn (:source t))})))})
  ([game-state tilemap-id make-tileset-id-fn tsj-tilemap]
   (assoc-in game-state tilemap-id (make-tilemap make-tileset-id-fn tsj-tilemap))))

(defn gid->id
  [gid first-gid]
  (- gid first-gid))

(defn compute-tile-metadata
  "Computes metadata for every tile that belongs to the `layer` to reduce
   computations during the rendering time.

  There are a few steps needed to get rendering data for each tile:

  1. Get tile GID from the layer it belongs to.
  2. Find out which tileset this GID is assigned to. This gives us tileset ID and first-gid.
  3. Compute the local tile ID from the GID and first-gid of the tileset.
  4. Get tileset from the global game state using the tileset ID from the step 2.
  5. Get spritesheet image ID from the tileset.
  6. Get spritesheet image from the global game state using the ID from the step 5.
  7. Get the tile's row and column from the local ID.
  8. Get the tile's X and Y coordinates in the image.
  9. Get the tile's width and height.

  This is a long list of steps which have to be repeated for every tile in a layer.
  Most of these steps could be precomputed and stored in the tilemap for access during
  the rendering phase.

  There are two options:

  1. Precompute everything and store maps of the tile data instead of GIDs in each layer.
  This allows for fastest rendering possible. The cost is that we need to reload everything
  in the map if any of the assets changed. It also makes tilemap loading more complicated.
  2. Precompute tile local IDs and their tileset IDs. The rendering will be slower than with
  the first option but the game can now reload spritesheets, tilesets, and maps independently
  each time any of them changes, which makes development process easier.

  In the second case the metadata has the following structure:

  ```
  ::tiles
   ┌───┐                               ::tile-metadata
   │ 1─┼──────┐  ┌───┬──────────────────────────────────────────────────────┐
   │ 3─┼─────┐│  │ 1 │  ::local-id  0                                       │
   │ 1 │     │└──▶   │::tileset-id  [:molfar/tilesets \"resources/decor.tsj\"]│
   │ 1 │     │   ├───┼──────────────────────────────────────────────────────┤
   │ 0 │     │   │ 2 │  ::local-id  1                                       │
   │ 0 │   ┌─┼───▶   │::tileset-id  [:molfar/tilesets \"resources/decor.tsj\"]│
   │ 9─┼─┐ │ │   ├───┼──────────────────────────────────────────────────────┤
   │ 9 │ │ │ │   │ 3 │  ::local-id  2                                       │
   │ 9 │ │ │ └───▶   │::tileset-id  [:molfar/tilesets \"resources/decor.tsj\"]│
   │ 9 │ │ │     ├───┼──────────────────────────────────────────────────────┤
   │ 2─┼─┼─┘     │ 9 │  ::local-id  1                                       │
   │ 2 │ └───────▶   │::tileset-id  [:molfar/tilesets \"resources/floor.tsj\"]│
   │ 0 │         └───┴──────────────────────────────────────────────────────┘
   └───┘
  ```"
  [tilemap layer])

(defn find-tile-tileset
  "Returns a map of `::first-gid` and `::tileset-id` for the given tile
   global ID. Returns `nil` if `gid` refers to an empty tile.

   The Tiled editor documentation explains how to figure out which
   tileset a global tile ID belongs to:

   > To figure out which tileset a tile belongs to, find the tileset that
   > has the largest firstgid that is smaller than or equal to the tile’s
   > GID. Once you have identified the tileset, subtract its firstgid from
   > the tile’s GID to get the local ID of the tile within the tileset."
  [tilemap gid]
  (when-not (tl/empty-tile? gid)
    (->> (::tilesets tilemap)
         (filter #(<= % gid))
         last)))

(defn gid->id
  "Returns local tile ID for the given global tile ID and its
   `first-gid` taken from the tileset the tile belongs to."
  [first-gid gid]
  (- gid first-gid))

;;
;;(defn tile-id-at-xy
;;  "Returns tile id at the given X and Y coordinates for the given tilemap layer."
;;  [tilemap layer x y]
;;  (let [col (.floor js/Math (/ x (:tile-width  tilemap)))
;;        row (.floor js/Math (/ y (:tile-height tilemap)))]
;;    (tile-id layer col row)))
;;
;;(defn obstacle-at-xy?
;;  "Checks if there is an obstacle at the given coordinates in the game world."
;;  [tilemap spritesheet x y]
;;  (->> tilemap
;;       :layers
;;       (some #(ss/tile-property spritesheet
;;                                (tile-id-at-xy tilemap % x y)
;;                                :obstacle))
;;       boolean))
;;
;;(defn tile-column
;;  "Returns tile column at the given X-coordinate."
;;  [tilemap x]
;;  (.floor js/Math (/ x (:tile-width tilemap))))
;;
;;(defn tile-row
;;  "Returns tile row at the given Y-coordinate."
;;  [tilemap y]
;;  (.floor js/Math (/ y (:tile-height tilemap))))
;;
;;(defn tile-column-x
;;  "Returns tile column X-coordinate."
;;  [tilemap column]
;;  (* column (:tile-width tilemap)))
;;
;;(defn tile-row-y
;;  "Returns tile row Y-coordinate."
;;  [tilemap row]
;;  (* row (:tile-height tilemap)))
;;
;;(defn tilemap-width
;;  "Returns tilemap width in pixels."
;;  [tilemap]
;;  (* (:columns    tilemap)
;;     (:tile-width tilemap)))
;;
;;(defn tilemap-height
;;  "Returns tilemap height in pixels."
;;  [tilemap]
;;  (* (:rows        tilemap)
;;     (:tile-height tilemap)))
;;
;;(defn tilemap-layer-width
;;  "Returns tilemap layer width in pixels."
;;  [layer spritesheet]
;;  (* (:columns    layer)
;;     (:tile-width spritesheet)))
;;
;;(defn tilemap-layer-height
;;  "Returns tilemap layer height in pixels."
;;  [layer spritesheet]
;;  (* (:rows        layer)
;;     (:tile-height spritesheet)))
;;
;;(defn tilemap-tile-layers
;;  "Returns tile layers for the given tilemap."
;;  [tilemap]
;;  (filter
;;   #(= (:type %) :tile-layer)
;;   (:layers tilemap)))
;;
;;(defn render-tilemap-layer!
;;  "Renders the given tilemap layer."
;;  [renderer layer spritesheet camera]
;;  (let [tile-width   (:tile-width  spritesheet)
;;        tile-height  (:tile-height spritesheet)
;;
;;        start-column (.floor js/Math (/ (:x camera) tile-width))
;;        start-row    (.floor js/Math (/ (:y camera) tile-height))
;;
;;        end-column   (+ start-column (/ (:width  camera) tile-width))
;;        end-row      (+ start-row    (/ (:height camera) tile-height))
;;
;;        offset-x     (- (* start-column tile-width)  (:x camera))
;;        offset-y     (- (* start-row    tile-height) (:y camera))]
;;    (doseq [r (range start-row    (+ 1 end-row))
;;            c (range start-column (+ 1 end-column))]
;;      (let [tile-id (tile-id layer c r)]
;;        (when-not (empty-tile? tile-id)
;;          (let [x (+ offset-x (* tile-width  (- c start-column)))
;;                y (+ offset-y (* tile-height (- r start-row)))]
;;            (ss/render-tile! renderer
;;                             spritesheet
;;                             tile-id
;;                             (.round js/Math x)
;;                             (.round js/Math y))))))))
;;
;;(defn render-tilemap!
;;  "Renders all tile layers of the given tilemap."
;;  [game-state renderer tilemap-id camera-id]
;;  (let [tilemap     (get-in game-state tilemap-id)
;;        spritesheet (get-in game-state (:spritesheet-id tilemap))
;;        camera      (get-in game-state camera-id)
;;        tile-layers (tilemap-tile-layers tilemap)] ;; TODO: filter out invisible layers?
;;    (r/clear! renderer)
;;    (doseq [layer tile-layers]
;;      (render-tilemap-layer! renderer layer spritesheet camera))))
