# CLJS-MOLFAR

This is a hobby game written in ClojureScript. The work is in progress. 
You can play the latest version of the game [online](https://manenko.gitlab.io/cljs-molfar/).

The API documentation is available [here](https://manenko.gitlab.io/cljs-molfar/doc/index.html).

## Development

To get an interactive development environment run:

    clj -A:fig:build

This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL.

To clean all compiled files:

    rm -rf target/public

To create a production build run:

    rm -rf target/public
    clj -A:fig:min


To generate API documentation in the `target/doc` directory:

    clj -X:codox

#### Emacs

Make sure you have Cider and then do `cider-jack-in-cljs`.

#### Cursive

Create a new `Clojure REPL/Remote` run configuration.
Choose `nREPL` 'Connection type' and 'Use port from nREPL file'
under the 'Connection details' section.
Then choose the project and 'Use standard port file'.

Start the REPL from the terminal:

    clj -A:fig:nrepl

Once this is done, run the configuration you created in
Cursive to connect to the remote REPL you started from
the terminal.
Finally, evaluate `(cljs-repl)` in the Cursive REPL.

### Palette

The game uses [resurrect-64](https://lospec.com/palette-list/resurrect-64) palette for its graphics.

## License

Copyright © 2022 Oleksandr Manenko

Distributed under the Apache License, Version 2.0.
