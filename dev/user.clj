;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns user)

(defn try-require
  "Like `require` but does not throw an exception in case of it
cannot load namespaces. Returns `true` if namespaces were loaded
successfully, `false` otherwise."
  [& args]
  (try
    (apply require args)
    true
    (catch Exception _
      false)))

(defn cljs-repl []
  "Starts CLJS REPL. This is needed for Cursive."
  (when (try-require 'figwheel.main.api)
    ((resolve 'figwheel.main.api/start) "dev")))

(defn generate-docs! []
  "Generates API documentation."
  (when (try-require '[codox.main])
    ((resolve 'codox.main/generate-docs)
           {:source-paths ["src"]
            :language     :clojurescript
            :metadata     {:doc/format :markdown}})))
